#include <Arduino.h>
#include "Motor.h"

/**
 * Motor pin and Servo initialization
 */
void Motor::setup(int motorPin) {
	_pin = motorPin;
	pinMode(motorPin, OUTPUT);
	_servo.attach(motorPin, 0, 180);
	_servo.write(90);
}

/**
 * The following function will adjust the selected motor's
 * speed according to the following:
 *
 * 0 - FULL SPEED in one direction
 * 90 - STOPPED
 * 180 - FULL Speed in the other direction
 */
void Motor::speedAdjust(int targetSpeed) {

	_servo.write(targetSpeed);

}

/**
 * Halt motor movement
 */
void Motor::stop() {

	this->speedAdjust(90);

}
