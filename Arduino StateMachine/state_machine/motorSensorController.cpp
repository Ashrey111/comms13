#include <Arduino.h>
#include "motorSensorController.h"
#include "Motor.h"

// Pin Declaration
static int motorPin = 0;

/**
 * Main Setup Code for Sensors / Motors
 * SD Initialization
 */
void motorSensorControllerSetup() {

	// Motor Initialization
	//motor.setup(motorPin);

	// Sensor Initialization
	// ...

}

int moveToLevel(int targetLevel) {

	Serial.println("Elevator is currently moving to the next level");

	return 0;

}

/**
 * Stop elevator movement in the event of an emergency
 */
void emergencyStop() {

	motor.stop();

}

void setLevel(int desiredLevel) {

	targetLevel = desiredLevel;

}

int getLevel() {

	return currentLevel;

}
