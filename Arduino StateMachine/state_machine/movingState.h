#pragma once
#include "state.h"

class movingState :
	public state
{
public:
	virtual void enter();
	virtual int exec();
	void inputHandler(char input);
	unsigned long timeFloorInputExec;
	unsigned long elapsedTimeFloorExec;
private:
	int toFloor;

	int progress;

};
