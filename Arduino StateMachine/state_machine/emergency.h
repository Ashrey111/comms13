#pragma once
#include "state.h"
class emergency :
	public state
{
public:
	void enter();
	int exec();
	void inputHandler(char input);

	unsigned long timeEmergency2;
	unsigned long elapsedTimeEmergency;
};
