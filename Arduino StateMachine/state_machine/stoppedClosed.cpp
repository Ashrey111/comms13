#include "stoppedClosed.h"
void stoppedClosed::enter() {
	//Get function to return current floor
	//atFloor=getFloor();
	//std::cout << "Current State: Stopped, doors closed\n";
	Serial.println("Stopped, doors closed");
}

int stoppedClosed::exec() {
	//TODO: Make decisions based on queue
	if (emergency) {
		return EMERGENCY;
	}
	if (queue[0] != 0) {
		if (getLevel() + queue[0] > 0 && getLevel() + queue[0] < 6) {
			return MOVING;
		}else {
			Serial.print("CAN'T GO TO LEVEL ");
			Serial.print(getLevel() + queue[0]); 
			Serial.print(" in direction "); 
			Serial.println(queue[0]);
			queue[0] = 0;
		}
	}
	return STOPPED_CLOSED;
}
void stoppedClosed::inputHandler(char input) {
	switch (input) {
	case '0': emergency = true;
		Serial.println("Emergency Input Received");
		timeEmergency = millis();
		break;
	case '1': addQueue(-1);
		Serial.println("Direction input received: Move Down");
    timeFloorInput = millis();
		break;
	case '2': addQueue(1);
		Serial.println("Direction input received: Move Up");
		timeFloorInput = millis();
		break;
	/* POST MVP
	case 3: //open door
		break;
	*/
	}
}
