// NOTE: When porting to arduino, replace all iostream functions with Serial equivalents (cin=Serial.read(), cout=Serial.println() or Serial.print())


#include <Arduino.h>
#include "State_Controller.h"
State_Controller *controller;

void setup() {
  controller=new State_Controller();
  controller->init();
  
}

void loop() {
  
	controller->execute();
  
}
