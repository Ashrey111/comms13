#pragma once
#include <Arduino.h>
#include <Servo.h>

class Motor {
	public:
	  void setup(int motorPin);
	  void speedAdjust(int targetSpeed);
	  void stop();

	private:
	  int _pin;
	  Servo _servo;
};
