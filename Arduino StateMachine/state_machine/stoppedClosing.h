#pragma once
#include "state.h"
class stoppedClosing :
	public state
{
public:
	void enter();
	int exec();
	void inputHandler(char input);
};

