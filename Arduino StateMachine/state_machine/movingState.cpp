#include "movingState.h"
void movingState::enter(){
	toFloor = getLevel()+queue[0];
	queue[0] = 0;
	// Performance Req: logging time taken to execute floor inputs
	timeFloorInputExec = millis();
	elapsedTimeFloorExec = timeFloorInputExec - timeFloorInput;
	Serial.print("Time taken to receive floor input: ");
	Serial.println(elapsedTimeFloorExec);
}
int movingState::exec() {
	if (emergency) {
		return EMERGENCY;
	}
	//int situation= speedAdjust(floor)
	if(moveToLevel(toFloor)==0){
		return STOPPED_OPEN;
	}
	return MOVING;
	
}
void movingState::inputHandler(char input) {
	switch (input) {
	case '0': emergency = true;
		Serial.println("Emergency Input Received");
		timeEmergency = millis();
		break;
	/* POST MVP
	case '1': addQueue(1);
		break;
	case '2': addQueue(-1);
		break;
	*/
	}
}
