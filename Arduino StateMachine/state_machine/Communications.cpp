#include <Arduino.h>
#include <string.h>
#include "Communications.h"

// Uno: { TX: 9, RX: 8 }

void Communications::initiateConnection() {
	Serial.begin(9600);
	Serial2.begin(9600);
	
	Serial.println("Arduino Uno: Serial started at 9600 Baud.");

	Serial.print("File:   ");
	Serial.println(__FILE__);
	Serial.print("Uploaded: ");
	Serial.println(__DATE__);
	Serial.println(" ");
}

void Communications::send(String data) {
	// data = addChecksum(data); // add checksum for validity
	for (int i = 0; i < data.length(); i++) {
		Serial.print(data[i]);
		Serial2.write(data[i]);
	};
	Serial.print("\n");
}

String Communications::receive() {
    if (Serial2.available()) {
		String temp = "";
	    while (Serial2.available()) {
			delay(10);
			char c = Serial2.read();
			temp.concat(c);
	  	};
		
		// ensure data is correct
		Serial.print("RECEIVED: ");
		Serial.print(temp + "\n");
		return temp;
  	};
	return "";
}

// add a checksum for even parity
String Communications::addChecksum(String message) {
	int sum = 0;
	String sumChar = "1";
	for (int i = 0; i < message.length(); i++) {
		char c = message[i];
		sum += c % 2;
	}
	if (sum % 2 == 0) {
		sumChar = "0";
	}
	String messageOut = message;
	messageOut += (sumChar);
	return messageOut;
}

// checksum calc to check the message for even parity
boolean Communications::calcChecksum(String message) {
	int sum = 0;
	for (size_t i = 0; i < message.length(); i++) {
		char c = message[i];
		sum += static_cast<int>(c) & 1;
	};
	return !(sum & 1);
}