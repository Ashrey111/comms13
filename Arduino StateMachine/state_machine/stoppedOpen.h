#pragma once
#include "state.h"
class stoppedOpen :
	public state
{
public:
	void enter();
	int exec();
	void inputHandler(char input);
private:
	int timeout=0;
};

