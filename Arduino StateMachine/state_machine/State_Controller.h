#pragma once
#include "state.h"
#include "stateDefs.h"


class State_Controller{
public:
  void init();
  void execute();
  
private:
  int stateID;
  state* states[state_count];
  String serialRecieve();
  String filterInput(String input);
};
