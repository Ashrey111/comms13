#include <Arduino.h>

static struct Communications {

	void initiateConnection();

	void send(String data);
	String receive();
	
	String addChecksum(String message);
	boolean calcChecksum(String message);

} Communications;