/*
 * readInput.h
 *
 * Created: 09/09/2019 21:36:11
 *  Author: Matthew
 */ 

#ifndef _READINPUT_H_
#define _READINPUT_H_

void initReadInput();
unsigned char inputDebounce();
unsigned char * readPins();

#endif