#include "stoppedClosing.h"
void stoppedClosing::enter() {
	//std::cout << "Current State: Stopped, doors closing\n";
	Serial.println("Stopped, doors closing");
}

int stoppedClosing::exec() {
	if (emergency) {
		return EMERGENCY;
	}
	//Check if doors closed yet: POST MVP
	return STOPPED_CLOSED;
}
void stoppedClosing::inputHandler(char input) {
	
	switch (input) {
	case '0': emergency = true;
		Serial.println("Emergency Input Received");
		timeEmergency = millis();
		break;
	/* POST MVP:
	case '0': addQueue(-1);
		break;
	case '2': addQueue(1);
		break;
	case '3': //open door
		break;
	*/
	}
	
}
