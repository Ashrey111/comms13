#pragma once
#include "Motor.h"

	static Motor motor;
	static int currentLevel;
	static int targetLevel;

	void motorSensorControllersetup();
	int moveToLevel(int desiredLevel);
	void setLevel(int targetLevel);
	int getLevel();
	void emergencyStop();

//class motorSensorController {
//	public:
//	  void setup();
//	  int moveToLevel(int targetLevel);
//	  void emergencyStop();
//	  int getLevel();
//
//	private:
//	  void setLevel(int level) { this->currentLevel = level; };
//	  int currentLevel;
//	  int targetLevel;
//	  Motor _motor;
//};
