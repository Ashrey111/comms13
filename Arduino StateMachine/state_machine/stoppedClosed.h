#pragma once
#include "state.h"
class stoppedClosed :
	public state
{
public:
	void enter();
	int exec();
	void inputHandler(char input);

};
