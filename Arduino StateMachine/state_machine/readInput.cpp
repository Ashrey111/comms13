/*
 * readInput.c
 *
 * Created: 09/09/2019 21:36:02
 *  Author: Matthew
 */ 

#include <avr/io.h>
#include "readInput.h"
#include "UIDEFINE.h"

/*
	void initReadInput()
	Params: none
	returns: none
	
	Info: initalises the port and pins used to read button inputs
*/
void initReadInput()
{
	//Assuming PORTA is used
	
	
	PORTC |= 0x00;	//Turn all pins on PORTA high
}

/*
	unsigned char inputDebounce()
	Params: none
	Returns: Unsigned char; 1 = okay to read next input, 0 = not okay to read next input
	
	Info:	Software debouncing, Checks to see whether the time since the last button press has surpassed a specified time delay
			returns 1 if so and okay to read button input, 0 no, don't read input yet.
*/
unsigned char inputDebounce()
{
	// static unsigned long lastTime = millis();
	
	// if((millis() - lastTime) > READINPUTTIMEDELAY)	//check if the timer has exceeded the set delay length
	// {
	// 	return 1;
	// }
		
	return 0;
}

/*
	Unsigned char *readPins()
	Params:	none
	Returns: pointer to pins
	
	Problems:	-	Needs to be updated to work for NMVP/accepting more than one port
				-	Dependinsg on complexity, might need to change to interrupt based
*/
unsigned char * readPins()
{
	unsigned char i;
	static unsigned char pins[READINPUTNUMPINS] = {0};
	
	for(i = 0; i < READINPUTNUMPINS; i++)
	{
		pins[i] = (PINC & (1 << i)) >> i;	//reads input from port, converts the read pin to 1 then adds it to the array at index position
	}
	return pins;
	
	
}