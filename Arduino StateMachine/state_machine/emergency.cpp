#include "emergency.h"

void emergency::enter() {
	//std::cout << "Current state: Emergency!\n";
	Serial.println("Emergency - stopping");

  	//Stop the lift
	emergencyStop();
	// Performance Req: Measure elapsed time for Emergency State to execute
	timeEmergency2 = millis();
	elapsedTimeEmergency = timeEmergency2 - timeEmergency;
	Serial.println("Elapsed Time to execute Emergency state: ");
	Serial.print(elapsedTimeEmergency);
}
int emergency::exec() {
	//No loop. Only execute once and make the arduino do nothing
}
void emergency::inputHandler(char input) {
	//Don't do anything lole
}
