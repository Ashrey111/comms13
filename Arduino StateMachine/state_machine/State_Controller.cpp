// State_Machine.cpp : This file contains the 'main' function. Simulates function calls of setup and loop of arduino
// NOTE: When porting to arduino, replace all iostream functions with Serial equivalents (cin=Serial.read(), cout=Serial.println() or Serial.print())

#include "State_Controller.h"
#include "Communications.h"
void State_Controller::init() {
 
	//boot code goes here
	//create state classes and put their references into the states array
		//https://stackoverflow.com/questions/33123371/how-to-create-array-of-an-abstract-class-in-c/33123456
	//go to nearest floor
  Communications.initiateConnection();

	states[STOPPED_CLOSED] = new stoppedClosed(); 
	states[MOVING] = new movingState();
	states[EMERGENCY] = new emergency();
	states[STOPPED_OPEN] = new stoppedOpen();
	states[STOPPED_CLOSING] = new stoppedClosing();

	stateID = STOPPED_CLOSED; //Finished boot, car ready for service
  Serial.println("Starting state machine");
	
}

void State_Controller::execute() {
	//Loop code goes here
	//Use terminal commands to simulate buttons being pressed or sensors being activated
  if (stateID != EMERGENCY) {
  	int currentStateID = stateID;
  
  	
	  String input = filterInput(Communications.receive());

    if(input==""){ //If no messages from UI, check for messages from Serial (laptop)
      input = filterInput(serialRecieve());
      // Logging for Serial inputs 
      if(input.length() > 0){
        // these prints statments print twice?
        Serial.print("RECEIVED INPUT: "); 
        Serial.println(input);  
      }   
    }

    if(input!=""){
      Serial.println(input);
      for(int i = 0; i < sizeof(input); i++){
        char inChar = input[i];
        states[stateID]->inputHandler(inChar);//Input handler for state   
      }
    }
	  
  	stateID= states[stateID]->exec();

  	if (stateID != currentStateID && stateID != OFF) { //if stateID has changed after exec
  		states[stateID]->enter();
  	}
	}
	
}

String State_Controller::serialRecieve(){ //For debugging purposes: if not communicating with UI, because we don't have a 2nd arduino or otherwise
  String fullInput="";
  while (Serial.available() > 0) {
    // read the incoming byte:
    char incomingByte = Serial.read();

    // say what you got:
    fullInput+=incomingByte;
  } 
  return fullInput;
  
}

String State_Controller::filterInput(String input){
  String fullInput="";
  for(char incoming: input){
    // read the incoming byte:
    if(incoming=='0'){
      return "0";
    }
    for(char input: fullInput){
      if(incoming == input){
        incoming = 'N';
      }
    }
    // say what you got:
    if(incoming != 'N'){  
      fullInput+=incoming;
    }
  } 
  return fullInput;
}
