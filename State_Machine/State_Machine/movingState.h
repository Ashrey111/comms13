#pragma once
#include "state.h"
class movingState :
	public state
{
public:
	virtual void enter();
	virtual int exec();
	void inputHandler(int input);
private:
	int toFloor;

	int progress;

};

