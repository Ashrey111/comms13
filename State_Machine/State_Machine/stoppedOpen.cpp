#include "stoppedOpen.h"
#define TIMEOUT_SECONDS 0 //Change to higher value post-mvp
void stoppedOpen::enter() {
	std::cout << "Current State: Stopped, doors open\n";
	timeout = TIMEOUT_SECONDS;
}

int stoppedOpen::exec() {
	//TODO: Make decisions based on queue
	if (emergency) {
		return EMERGENCY;
	}
	//Check if timeout has been reached yet: POST MVP
	return STOPPED_CLOSING;
}
void stoppedOpen::inputHandler(int input) {
	
	switch (input) {
	case 0: emergency = true;
		break;
	/* POST MVP
	case 1: addQueue(-1);
		break;
	case 2: addQueue(1);
		break;
	case 4: //close door
		break;
	*/
	}
	
}
