#pragma once
#include "state.h"
class emergency :
	public state
{
public:
	void enter();
	int exec();
	void inputHandler(int input);

};

