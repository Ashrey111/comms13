// State_Machine.cpp : This file contains the 'main' function. Simulates function calls of setup and loop of arduino
// NOTE: When porting to arduino, replace all iostream functions with Serial equivalents (cin=Serial.read(), cout=Serial.println() or Serial.print())

#include <iostream>
#include "state.h"
#include "stateDefs.h"

int stateID;
state* states[state_count];


void setup() {
	//boot code goes here
	//create state classes and put their references into the states array
		//https://stackoverflow.com/questions/33123371/how-to-create-array-of-an-abstract-class-in-c/33123456
	//go to nearest floor
	states[STOPPED_CLOSED] = new stoppedClosed(); 
	states[MOVING] = new movingState();
	states[EMERGENCY] = new emergency();
	states[STOPPED_OPEN] = new stoppedOpen();
	states[STOPPED_CLOSING] = new stoppedClosing();

	stateID = STOPPED_CLOSED; //Finished boot, car ready for service

}

void loop() {
	//Loop code goes here
	//Use terminal commands to simulate buttons being pressed or sensors being activated
	
	int currentStateID = stateID;

	
	if (stateID != EMERGENCY) {
		std::string command;
		std::cin >> command;
		if (command == "queue") {
			states[stateID]->printQueue();
		}
		else if (command == "state") {
			//Print current state
			switch (stateID) {
			case EMERGENCY: std::cout << "Emergency!";
				break;
			case MOVING: std::cout << "Moving";
				break;
			case STOPPED_CLOSING: std::cout << "Stopped, closing";
				break;
			case STOPPED_CLOSED: std::cout << "Stopped, closed";
				break;
			case STOPPED_OPEN: std::cout << "Stopped, open";
				break;
			default: std::cout << "I dunno lole";
				break;
			}
			std::cout << "\n";
		}
		else { //Get numerical value
			int inputValue = command[0] - '0';
			states[stateID]->inputHandler(inputValue);//Input handler for state. When porting to arduino, change this to wire.onRecieve(inputHandler). Will undergo MASSIVE changes when porting so just keep that in mind. 

		}
	}

	stateID= states[stateID]->exec();
	
	if (stateID == OFF) {
		std::cout << "Turning off...\n"; //Replace with turn off or standby command for porting
		return;
	}

	if (stateID != currentStateID && stateID != OFF) { //if stateID has changed after exec
		states[stateID]->enter();
	}

	
	
}




// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file


//Ignore everything below this line
int main()
{
	std::cout << "Booting...\n";
	setup();
	std::cout << "Running main loop...\n";
	while (stateID != OFF) {
		loop();
	}
	
	return 0;
}