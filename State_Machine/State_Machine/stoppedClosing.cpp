#include "stoppedClosing.h"
void stoppedClosing::enter() {
	std::cout << "Current State: Stopped, doors closing\n";
}

int stoppedClosing::exec() {
	if (emergency) {
		return EMERGENCY;
	}
	//Check if doors closed yet: POST MVP
	return STOPPED_CLOSED;
}
void stoppedClosing::inputHandler(int input) {
	
	switch (input) {
	case 0: emergency = true;
		break;
	/* POST MVP:
	case 1: addQueue(-1);
		break;
	case 2: addQueue(1);
		break;
	case 3: //open door
		break;
	*/
	}
	
}
