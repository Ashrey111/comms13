#include "stoppedClosed.h"
void stoppedClosed::enter() {
	//Get function to return current floor
	//atFloor=getFloor();
	std::cout << "Current State: Stopped, doors closed\n";
}

int stoppedClosed::exec() {
	//TODO: Make decisions based on queue
	if (emergency) {
		return EMERGENCY;
	}
	if (queue[0] != 0) {
		if (atFloor + queue[0] > 0 && atFloor + queue[0] < 6) {
			return MOVING;
		}else {
			std::cout << "CAN'T GO TO FLOOR " << atFloor + queue[0] << "\n";
			queue[0] = 0;
		}
	}
	return STOPPED_CLOSED;
}
void stoppedClosed::inputHandler(int input) {
	switch (input) {
	case 0: emergency = true;
		break;
	case 1: addQueue(-1);
		break;
	case 2: addQueue(1);
		break;
	/* POST MVP
	case 3: //open door
		break;
	*/
	}
}

