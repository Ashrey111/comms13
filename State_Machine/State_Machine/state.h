#pragma once
#include <arduino.h>
#define QUEUE_SIZE 5
enum ids { OFF = -1, EMERGENCY, STOPPED_CLOSED, STOPPED_OPEN, STOPPED_CLOSING, MOVING, state_count }; //count will always be the number of states as long as its the last enum

class state
{
public: 
	virtual void enter() =0;
	virtual int exec()=0; 
	virtual void inputHandler(int input) = 0;
	void printQueue();
	void addQueue(int floor);
	void printFloor();
	
protected:
	static int queue[5];
	static int atFloor;
	static bool emergency;
};

